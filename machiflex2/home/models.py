from django.db import models

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.


## T E C N I C O

class Tecnico(models.Model):
    Numero = models.AutoField(primary_key=True, unique=True, editable=False)
    NomPila = models.CharField(max_length=30, null=False)
    PrimApell = models.CharField(max_length=30, null=False)
    SegApell = models.CharField(max_length=30, null=True)
    NumTel = models.CharField(max_length=15, null=False)
    Correo = models.CharField(max_length=60, null=False)
    Usuario = models.CharField(max_length=60, null=False)
    Contraseña = models.CharField(max_length=20, null=False)
    Status=models.BooleanField(default=True)


    def __str__(self):
        return str(self.Numero)


##  U S U A R I O S

class usuarios(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    Numero = models.AutoField(primary_key=True, unique=True, editable=False)
    Nombre = models.CharField(max_length=50, null=True)
    NumTel = models.CharField(max_length=15, null=False)
    ContNomPila = models.CharField(max_length=30, null=False)
    ContPrimApell = models.CharField(max_length=30, null=False)
    ContSegApell = models.CharField(max_length=30, blank=True, null=True)
    Colonia = models.CharField(max_length=50, null=False)
    Calle = models.CharField(max_length=50, null=False)
    Cp = models.IntegerField(null=True)

    def __str__(self):
        if self.Nombre:
            return self.Nombre
        else:
            return f"Usuario sin nombre - ID: {self.Numero}"
        
## T I P O   M A Q U I N A

class TipoMaquina(models.Model):
    Codigo = models.CharField(primary_key=True, unique=True, max_length=5)
    Nombre = models.CharField(max_length=100, null=False)
    Descripcion = models.CharField(max_length=150, null=False)

    def __str__(self):
        return self.Nombre

## C A M B I O

class Cambio(models.Model):
    Codigo = models.CharField(primary_key=True, max_length=5)
    Descripcion = models.CharField(max_length=200, null=False)

    def __str__(self):
        return self.Codigo

## M A R C A

class Marca(models.Model):
    Codigo = models.CharField(primary_key=True, max_length=5)
    Nombre = models.CharField(max_length=30, null=False)

    def __str__(self):
        return self.Nombre

## M O D E L O

class Modelo(models.Model):
    Codigo = models.CharField(primary_key=True, max_length=5)
    Nombre = models.CharField(max_length=30, null=False)
    Año = models.IntegerField(null=True)
    Marca = models.ForeignKey(Marca, on_delete=models.CASCADE)

    def __str__(self):
        return self.Nombre

## M A Q U I N A

class Maquina(models.Model):
    Codigo = models.CharField(primary_key=True, max_length=5)
    Descripcion = models.CharField(max_length=100, null=True)
    Tipo_Maquina = models.ForeignKey(TipoMaquina, on_delete=models.CASCADE)
    Marca = models.ForeignKey(Marca, on_delete=models.CASCADE)
    Modelo = models.ForeignKey(Modelo, on_delete=models.CASCADE)

    def __str__(self):
        return self.Codigo

##  C I T A

class Cita(models.Model):
    Numero = models.AutoField(primary_key=True)
    Fecha = models.DateField(null=False)
    Hora = models.CharField(max_length=10, null=False)
    Tecnico = models.ForeignKey(Tecnico, on_delete=models.CASCADE)
    Usuario = models.ForeignKey(usuarios, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.Numero)

## S E R V I C I O

class Servicio(models.Model):
    Numero = models.AutoField(primary_key=True, unique=True)
    Descripcion = models.CharField(max_length=200, null=False)
    Maquina = models.ForeignKey(Maquina, on_delete=models.CASCADE)
    Cita = models.ForeignKey(Cita, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.Numero)

## S E R V I C I O   C A M B I O

class ServCambio(models.Model):
    Servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    Cambio = models.ForeignKey(Cambio, on_delete=models.CASCADE)
    Cantidad = models.IntegerField(null=False)

    class Meta:
        unique_together = ('Servicio', 'Cambio')

## U B I C A C I O N
class Ubicacion(models.Model):
    direccion = models.CharField(max_length=255)

    def __str__(self):
        return self.direccion