from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms
from .models import *


## U S U A R I O S

class SignUpForm(UserCreationForm):
    Nombre = forms.CharField(max_length=50, label="Nombre (Moral)", widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Ingrese nombre moral"}))
    NumTel = forms.CharField(max_length=15,label="Número teléfonico", widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Ingrese número de teléfono"}))
    ContNomPila = forms.CharField(max_length=30, label="Nombre de pila", widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Ingrese su nombre de pila"}))
    ContPrimApell = forms.CharField(max_length=30, label="Primer apellido", widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Ingrese su primer apellido"}))
    ContSegApell = forms.CharField(max_length=30, label="Segundo apellido", required=False, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Ingrese su segundo apellido"}))
    Colonia = forms.CharField(max_length=50, label="Colonia", required=False, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Ingresa la colonia"}))
    Calle = forms.CharField(max_length=50, label="Calle", required=False, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Ingrese la calle"}))
    Cp = forms.IntegerField(label="Código Postal", required=True, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Ingrese el código postal"}))
    username = forms.CharField(max_length=32, label="Nombre de usuario", widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Ingrese su username"}))
    password1 = forms.CharField(max_length=32, label="Contraseña", widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Ingrese su contraseña"}))
    password2 = forms.CharField(max_length=32, label="Confirmar contraseña", widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Repita su contraseña"}))
    class Meta:
        model = User
        fields = [
            "Nombre",
            "NumTel",
            "ContNomPila",
            "ContPrimApell",
            "ContSegApell",
            "Colonia",
            "Calle",
            "Cp",
            "username",
            "password1",
            "password2"
        ]

class ClienteForm(forms.ModelForm):
    class Meta:
        model = usuarios
        fields = "__all__"
        exclude = ["user", "Numero"]
        widgets = {
            "Nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del cliente si es moral"}),
            "NumTel": forms.TextInput(attrs={"class": "form-control", "placeholder": "Número telefónico del cliente"}),
            "ContNomPila": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre pila del contacto"}),
            "ContPrimApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Primer apellido del contacto"}),
            "ContSegApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Segundo apellido del contacto"}),
        }

class UpdateClienteForm(forms.ModelForm):
    class Meta:
        model = usuarios
        fields = "__all__"
        exclude = ["user", "Numero"]
        labels = {
            "Nombre": "Nombre (Moral)",
            "NumTel": "Número teléfonico",
            "ContNomPila": "Nombre de pila",
            "ContPrimApell": "Primer apellido",
            "ContSegApell": "Segundo apellido",
            "Colonia": "Colonia",
            "Calle": "Calle",
            "Cp": "Código Postal",
        }

        widgets = {
            "Nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del cliente si es moral"}),
            "NumTel": forms.TextInput(attrs={"class": "form-control", "placeholder": "Número telefónico del usuario"}),
            "ContNomPila": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre pila del usuario"}),
            "ContPrimApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Primer apellido del usuario"}),
            "ContSegApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Segundo apellido del usuario"}),
            "Colonia": forms.TextInput(attrs={"class": "form-control", "placeholder": "Colonia del usuario"}),
            "Calle": forms.TextInput(attrs={"class": "form-control", "placeholder": "Calle del usuario"}),
            "Cp": forms.TextInput(attrs={"class": "form-control", "placeholder": "Código postal del usuario"}),
        }



class LoginForm(forms.Form):
    Username = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu username"}))
    Contraseña = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escribe tu contraseña"}))


## S E R V I C I O S

class ServiciosForm(forms.ModelForm):
    class Meta:
        model = Servicio
        fields = "__all__"
        exclude = ["Numero"]
        widgets = {
            "Descripcion": forms.TextInput(attrs={"class": "form-control", "placeholder": "Descripcion de servicio"}),
            "Maquina": forms.Select(attrs={"type":"select","class":"form-select"}),
            "Cita": forms.Select(attrs={"type":"select","class":"form-select"}),
        }
    
class UpdateServiciosForm(forms.ModelForm):
    class Meta:
        model = Servicio
        fields = "__all__"
        exclude = ["Numero"]
        widgets = {
            "Descripcion": forms.TextInput(attrs={"class": "form-control", "placeholder": "Descripcion de servicio"}),
            "Maquina": forms.Select(attrs={"type":"select","class":"form-select"}),
            "Cita": forms.Select(attrs={"type":"select","class":"form-select"}),
        }

## S E R V C A M B I O S
    
class ServCambForm(forms.ModelForm):
    class Meta:
        model = ServCambio
        fields = "__all__"
        widgets = {
            "Servicio": forms.Select(attrs={"type": "select", "class": "form-select"}),
            "Cambio": forms.Select(attrs={"type": "select", "class": "form-select"}),
            "Cantidad": forms.TextInput(attrs={"class": "form-control", "placeholder": "Cantidad de cambios"}),
        }
    
class UpdateServCambForm(forms.ModelForm):
    class Meta:
        model = ServCambio
        fields = "__all__"
        widgets = {
            "Servicio": forms.Select(attrs={"type": "select", "class": "form-select"}),
            "Cambio": forms.Select(attrs={"type": "select", "class": "form-select"}),
            "Cantidad": forms.TextInput(attrs={"class": "form-control", "placeholder": "Cantidad de cambios"}),
        }

##  C A M B I O S

class CambiosForm(forms.ModelForm):
    class Meta:
        model = Cambio
        fields = "__all__"
        widgets = {
            "Codigo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Codigo del cambio"}),
            "Descripcion": forms.TextInput(attrs={"class": "form-control", "placeholder": "Descripción del cambio"}),
        }


class UpdateCambiosForm(forms.ModelForm):
    class Meta:
        model = Cambio
        fields = "__all__"
        widgets = {
            "Codigo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Codigo del cambio"}),
            "Descripcion": forms.TextInput(attrs={"class": "form-control", "placeholder": "Descripción del cambio"}),
        }

## M A Q U I N A S


class MaquinaForm(forms.ModelForm):
    class Meta:
        model = Maquina
        fields = "__all__"
        widgets = {
            "Codigo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Codigo del la máquina"}),
            "Descripcion": forms.TextInput(attrs={"class": "form-control", "placeholder": "Descripción del la máquina"}),
            "Tipo_Maquina": forms.Select(attrs={"type": "select", "class": "form-select"}),
            "Marca": forms.Select(attrs={"type": "select", "class": "form-select"}),
            "Modelo": forms.Select(attrs={"type": "select", "class": "form-select"}),
        }


class UpdateMaquinaForm(forms.ModelForm):
    class Meta:
        model = Maquina
        fields = "__all__"
        widgets = {
            "Codigo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Codigo del la máquina"}),
            "Descripcion": forms.TextInput(attrs={"class": "form-control", "placeholder": "Descripción del la máquina"}),
            "Tipo_Maquina": forms.Select(attrs={"type": "select", "class": "form-select"}),
            "Marca": forms.Select(attrs={"type": "select", "class": "form-select"}),
            "Modelo": forms.Select(attrs={"type": "select", "class": "form-select"}),
        }


## T E C N I C O S


class TecnicosForm(forms.ModelForm):
    class Meta:
        model = Tecnico
        fields = "__all__"
        exclude = ["Numero"]
        widgets = {
            "NomPila": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre de pila"}),
            "PrimApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Primer apellido"}),
            "SegApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Segundo apellido"}),
            "NumTel": forms.TextInput(attrs={"class": "form-control", "placeholder": "Número teléfonico"}),
            "Correo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Correo"}),
            "Usuario": forms.TextInput(attrs={"class": "form-control", "placeholder": "Usuario"}),
            "Contraseña": forms.TextInput(attrs={"class": "form-control", "placeholder": "Contraseña"}),
        }


class UpdateTecnicosForm(forms.ModelForm):
    class Meta:
        model = Tecnico
        fields = "__all__"
        exclude = ["Numero"]
        widgets = {
            "NomPila": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre de pila"}),
            "PrimApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Primer apellido"}),
            "SegApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Segundo apellido"}),
            "NumTel": forms.TextInput(attrs={"class": "form-control", "placeholder": "Número teléfonico"}),
            "Correo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Correo"}),
            "Usuario": forms.TextInput(attrs={"class": "form-control", "placeholder": "Usuario"}),
            "Contraseña": forms.TextInput(attrs={"class": "form-control", "placeholder": "Contraseña"}),
        }

class UpdateUsersForm(forms.ModelForm):
    class Meta:
        model = usuarios
        fields = "__all__"
        exclude = ["user", "Numero"]
        widgets = {
            "Nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre Moral"}),
            "NumTel": forms.TextInput(attrs={"class": "form-control", "placeholder": "Número teléfonico"}),
            "ContNomPila": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre de pila"}),
            "ContPrimApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Primer apellido"}),
            "ContSegApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Segundo apellido"}),
        }

