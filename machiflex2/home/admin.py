from django.contrib import admin

# Register your models here.

from django.contrib import admin

from .models import usuarios, Tecnico, TipoMaquina, Cambio, Marca, Modelo, Maquina, Cita, Servicio, ServCambio

# Register your models here.


@admin.register(usuarios)
class UsuariosAdmin(admin.ModelAdmin):
    list_display = [
        "user",
        "Numero",
        "Nombre",
        "NumTel",
        "Colonia",
        "Calle",
        "Cp",
        "ContNomPila",
        "ContPrimApell",
        "ContSegApell"
    ]

@admin.register(Tecnico)
class TecnicoAdmin(admin.ModelAdmin):
    list_display = [
        "Numero",
        "NomPila",
        "PrimApell",
        "SegApell",
        "NumTel",
        "Correo",
        "Usuario",
        "Contraseña"
    ]

@admin.register(TipoMaquina)
class TipoMaquinaAdmin(admin.ModelAdmin):
    list_display = [
        'Codigo',
        'Nombre',
        'Descripcion'
    ]

@admin.register(Cambio)
class CambioAdmin(admin.ModelAdmin):
    list_display = [
        'Codigo',
        'Descripcion'
    ]

@admin.register(Marca)
class MarcaAdmin(admin.ModelAdmin):
    list_display = [
        'Codigo',
        'Nombre'
    ]

@admin.register(Modelo)
class ModeloAdmin(admin.ModelAdmin):
    list_display = [
        'Codigo',
        'Nombre',
        'Año',
        'Marca'
    ]

@admin.register(Maquina)
class MaquinaAdmin(admin.ModelAdmin):
    list_display = [
        'Codigo',
        'Descripcion',
        'Tipo_Maquina',
        'Marca',
        'Modelo'
    ]

@admin.register(Cita)
class CitaAdmin(admin.ModelAdmin):
    list_display = [
        'Numero',
        'Fecha',
        'Hora',
        'Tecnico',
        'Usuario'
    ]

@admin.register(Servicio)
class ServicioAdmin(admin.ModelAdmin):
    list_display = [
        'Numero',
        'Descripcion',
        'Maquina',
        'Cita'
    ]

@admin.register(ServCambio)
class ServCambioAdmin(admin.ModelAdmin):
    list_display = [
        'Servicio',
        'Cambio',
        'Cantidad'
    ]
