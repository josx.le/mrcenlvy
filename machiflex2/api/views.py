from .serializers import *
from home.models import *
from rest_framework import viewsets

from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse
from api.serializers import MyTokenObtainPairSerializer, RegisterSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.decorators import api_view, permission_classes
import json

# Create your views here.

##  T E C N I C O S 

class Tecnicojson(viewsets.ModelViewSet):
    serializer_class = SerializerTecnico
    queryset = Tecnico.objects.all()

##  U S U A R I O S
    
class Usuariosjson(viewsets.ModelViewSet):
    serializer_class = serializerUsuarios
    queryset = usuarios.objects.all()

## T I P O   M A Q U I N A

class Tmaquinajson(viewsets.ModelViewSet):
    serializer_class = serializerTmaquina
    queryset = TipoMaquina.objects.all()

## C A M B I O
    
class Cambiosjson(viewsets.ModelViewSet):
    serializer_class = serializerCambio
    queryset = Cambio.objects.all()

## M A R C A 
    
class Marcajson(viewsets.ModelViewSet):
    serializer_class = serializerMarca
    queryset = Marca.objects.all()

## M O D E L O
    
class Modelojson(viewsets.ModelViewSet):
    serializer_class = serializerModelo
    queryset = Modelo.objects.all()

## M A Q U I N A

class Maquinajson(viewsets.ModelViewSet):
    serializer_class = serializerMaquina
    queryset = Maquina.objects.all()  

##  C I T A

class Citajson(viewsets.ModelViewSet):
    serializer_class = serializerCita
    queryset = Cita.objects.all() 

## S E R V I C I O

class Serviciojson(viewsets.ModelViewSet):
    serializer_class = serializerServicio
    queryset = Servicio.objects.all() 

## S E R V I C I O   C A M B I O
    
class ServCambiojson(viewsets.ModelViewSet):
    serializer_class = serializerServCambio
    queryset = ServCambio.objects.all() 


## I D K 

class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class RegisterView(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer


@api_view(['GET'])
def getRoutes(request):
    routes = [
        '/api/token/',
        '/api/register/',
        '/api/token/refresh/',
        '/api/test/'
    ]
    return Response(routes)


@api_view(['GET', 'POST'])
@permission_classes([IsAuthenticated])
def testEndPoint(request):
    if request.method == 'GET':
        data = f"Congratulation {request.user}, your API just responded to GET request"
        return Response({'response': data}, status=status.HTTP_200_OK)
    elif request.method == 'POST':
        try:
            body = request.body.decode('utf-8')
            data = json.loads(body)
            if 'text' not in data:
                return Response("Invalid JSON data", status.HTTP_400_BAD_REQUEST)
            text = data.get('text')
            data = f'Congratulation your API just responded to POST request with text: {text}'
            return Response({'response': data}, status=status.HTTP_200_OK)
        except json.JSONDecodeError:
            return Response("Invalid JSON data", status.HTTP_400_BAD_REQUEST)
    return Response("Invalid JSON data", status.HTTP_400_BAD_REQUEST)