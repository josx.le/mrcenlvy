from home.models import *
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

##  T E C N I C O S 

class SerializerTecnico (serializers.ModelSerializer):
    class Meta:
        model = Tecnico
        fields = "__all__"
        
##  U S U A R I O S 
        
class serializerUsuarios (serializers.ModelSerializer):
    class Meta:
        model = usuarios
        fields = "__all__"

## T I P O   M A Q U I N A
        
class serializerTmaquina (serializers.ModelSerializer):
    class Meta:
        model = TipoMaquina
        fields = "__all__"
        
## C A M B I O

class serializerCambio (serializers.ModelSerializer):
    class Meta:
        model = Cambio
        fields = "__all__"

## M A R C A
        
class serializerMarca (serializers.ModelSerializer):
    class Meta:
        model = Marca
        fields = "__all__"

## M O D E L O
        
class serializerModelo (serializers.ModelSerializer):
    class Meta:
        model = Modelo
        fields = "__all__"

## M A Q U I N A
        
class serializerMaquina (serializers.ModelSerializer):
    class Meta:
        model = Maquina
        fields = "__all__"

##  C I T A

class serializerCita (serializers.ModelSerializer):
    class Meta:
        model = Cita
        fields = "__all__"

## S E R V I C I O

class serializerServicio (serializers.ModelSerializer):
    class Meta:
        model = Servicio
        fields = "__all__"

## S E R V I C I O   C A M B I O
        
class serializerServCambio (serializers.ModelSerializer):
    class Meta:
        model = ServCambio
        fields = "__all__"


## I D K 

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom claims
        token['username'] = user.username
        token['email'] = user.email
        # ...

        return token


class RegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('username', 'password', 'password2')

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError(
                {"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user