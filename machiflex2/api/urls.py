from django.urls import path, include
from rest_framework import routers
from . import views

from rest_framework_simplejwt.views import (
    TokenRefreshView,
)


app_name ="api"

router = routers.DefaultRouter()
router.register(r"jsonTecnicos", views.Tecnicojson)
router.register(r"jsonUsuarios", views.Usuariosjson)
router.register(r"jsonTmaquina", views.Tmaquinajson)
router.register(r"jsonCambio", views.Cambiosjson)
router.register(r"jsonMarca", views.Marcajson)
router.register(r"jsonModelo", views.Modelojson)
router.register(r"jsonMaquina", views.Maquinajson)
router.register(r"jsonCita", views.Citajson)
router.register(r"jsonServicio", views.Serviciojson)
router.register(r"jsonServCambio", views.ServCambiojson)

urlpatterns = [
    path('cruds/', include(router.urls)),


    path('token/', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('register/', views.RegisterView.as_view(), name='auth_register'),
    path('test/', views.testEndPoint, name='test'),
    path('', views.getRoutes)

]